# Generated by Django 4.1.6 on 2023-02-14 23:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('turnero', '0012_rol_ticket'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('usuario_id', models.AutoField(primary_key=True, serialize=False)),
                ('usuario_alias', models.CharField(max_length=50)),
                ('contrasena', models.CharField(max_length=50)),
                ('fecha_insercion', models.DateTimeField(null=True)),
                ('usuario_insercion', models.CharField(max_length=50, null=True)),
                ('fecha_modificacion', models.DateTimeField(null=True)),
                ('usuario_modificacion', models.CharField(max_length=50, null=True)),
                ('rol_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.rol')),
                ('trabajador_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.trabajador')),
            ],
            options={
                'db_table': 'Usuario',
            },
        ),
        migrations.CreateModel(
            name='Cola_espera',
            fields=[
                ('cola_id', models.AutoField(primary_key=True, serialize=False)),
                ('cola_desc', models.CharField(max_length=50)),
                ('fecha_insercion', models.DateTimeField(null=True)),
                ('usuario_insercion', models.CharField(max_length=50, null=True)),
                ('fecha_modificacion', models.DateTimeField(null=True)),
                ('usuario_modificacion', models.CharField(max_length=50, null=True)),
                ('cliente_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.cliente')),
                ('prioridad_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.prioridades')),
                ('puesto_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.puesto')),
                ('servicio_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.tipo_servicio')),
                ('ticket_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.ticket')),
            ],
            options={
                'db_table': 'Cola_espera',
            },
        ),
    ]
